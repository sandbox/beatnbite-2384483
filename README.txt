-= baguetteBox.js for inline images =-

Summary
=========================
This Drupal 8 module provides integration with BaguetteBox library and uses it to create slideshows from inline images.

Requirements
=========================
 * BaguetteBox should be installed to libraries/baguettebox directory.

Installation
=========================
 * Install as usual, see http://drupal.org/node/895232 for further information.
 * Download BaguetteBox library from https://github.com/feimosi/baguetteBox.js
 * Unzip the library and place files from dist directory to libraries/baguettebox directory.
