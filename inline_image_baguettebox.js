(function (drupalSettings) {

  'use strict';

  Drupal.behaviors.inlineImageBaguetteBox = {
    attach: function (context) {
      // The library can't work withing different contexts, so we trigger it on the document only
      if (context instanceof HTMLDocument) {
        baguetteBox.run('article.node', {
          captions: false,       // true|false|callback - Display image captions or return from callback (arguments: gallery, element)
          buttons: true,         // 'auto'|true|false - Display buttons
          async: true,           // true|false - Load files asynchronously
          preload: 2,            // [number] - How many files should be preloaded from current image
          animation: 'slideIn',  // 'slideIn'|'fadeIn'|false - Animation type
          afterShow: null,       // callback - To be run after showing the overlay
          afterHide: null        // callback - To be run after hiding the overlay
        });
      }
    }
  };

})(drupalSettings);
